@extends('layouts.app')

@section('content')
<style>

</style>

<div class="d-flex" id="wrapper">

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <div class="container-fluid py-4">

            <div class="col-md-12">
                <div class="row">

                    <div class="alert-group" style="width:100%">
                        @if (session()->has('success'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <strong>Success!</strong> {{ session('success') }}<span class="sucess"></span>
                        </div>
                        @endif
                        @if (session()->has('error'))
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <strong>Alert!</strong> {{ session('error') }}
                        </div>
                        @endif
                    </div>


                    <div class="cardtitle mb-20 col-md-12">
                        <h3 class="pull-left">Car Portal</h3>
                        <a href="#" class="pull-right weblink" data-toggle="modal" data-target="#addnewcar">
                            <button type="button" class="btn btn-primary">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add Car
                            </button>
                        </a>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Car Company</th>
                                        <th>Model</th>
                                        <th> Owner Name</th>
                                        <th>vehicle number</th>
                                        <th>Address</th>
                                        <th>pincode</th>
                                        <th>city</th>
                                        <th>state</th>
                                        <th>country</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($cardetail)
                                    @foreach($cardetail as $car)
                                    <tr>
                                        <td>{{$car->id}}</td>
                                        <td>{{$car->company_name}}</td>
                                        <td>{{$car->model}}</td>
                                        <td>{{$car->owner_name}}</td>
                                        <td>{{$car->vehicle_no}}</td>
                                        <td>{{$car->addressline}}</td>
                                        <td>{{$car->pincode}}</td>
                                        <td>{{$car->city}}</td>
                                        <td>{{$car->state}}</td>
                                        <td>{{$car->country}} </td>


                                        <td>
                                            <a href="#" class="weblink Editrecord" data-toggle="modal" data-target="#addnewcar"> <i class="fa fa-pencil color-green" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;
                                            <a href="{{route('delete', ['id'=>$car->id])}}" class="weblink" onclick="return confirm('do you want to delete Record '+ <?php echo $car->id ?> +'?')"> <i class="fa fa-trash color-red" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif


                                </tbody>
                            </table>
                        </div>

                        <div>
                        </div>





                        <!-- Modal -->
                        <div class="modal fade " id="addnewcar" tabindex="-1" role="dialog" aria-labelledby="addnewcarLabel" aria-hidden="true">
                            <div class="modal-dialog  modal-lg" role="document">

                                <form method="post" action="{{route('addcar')}}" id="formdata">
                                    <input type="hidden" name="id" id="id">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="addnewcarLabel">Add New Car Details</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">

                                            <div class="card-body">

                                                <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}" />
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="name">Company Name</label>
                                                        <input type="text" class="form-control" name="name" id="name" placeholder="">

                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="model">Model Name</label>
                                                        <input type="text" class="form-control" name="model" id="model" placeholder="">
                                                    </div>
                                                </div>


                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="owner">Owner Name</label>
                                                        <input type="text" class="form-control" name="owner" id="owner" placeholder="">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="vehicle">Vehicle Number</label>
                                                        <input type="text" class="form-control" name="vehicle" id="vehicle" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label for="address_1">Address Line </label>
                                                        <textarea name="address" id="address_1" cols="30" class="form-control"></textarea>
                                                    </div>

                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-2">
                                                        <label for="pincode">Pincode</label>
                                                        <input type="text" name="pincode" class="form-control" id="pincode" value="" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="state">State</label>
                                                        <input type="text" name="state" class="form-control" id="state" value="">
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="city">City</label>

                                                        <input type="text" name="city" class="form-control" id="city" value="">
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="country">Country</label>
                                                        <input type="text" name="country" class="form-control" id="country" value="">
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"> <i class="fa fa-times" aria-hidden="true"></i> Close</button>
                                            <button type="submit" class="btn btn-success">
                                                <i class="fa fa-check" aria-hidden="true"></i>Save</button>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
    </div>



    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="{{ asset('js/toaster.js') }}"></script>
    <script>
        $(document).ready(function() {
            $(".Editrecord").click(function() {
                let id = $("#id").val($(this).closest("tr").find("td:eq(0)").text());
                let company = $("#name").val($(this).closest("tr").find("td:eq(1)").text());
                let model = $("#model").val($(this).closest("tr").find("td:eq(2)").text());
                let owner = $("#owner").val($(this).closest("tr").find("td:eq(3)").text());
                let vehicle = $("#vehicle").val($(this).closest("tr").find("td:eq(4)").text());
                let address = $("#address_1").val($(this).closest("tr").find("td:eq(5)").text());
                let pincode = $("#pincode").val($(this).closest("tr").find("td:eq(6)").text());
                let city = $("#city").val($(this).closest("tr").find("td:eq(7)").text());
                let state = $("#state").val($(this).closest("tr").find("td:eq(8)").text());
                let country = $("#country").val($(this).closest("tr").find("td:eq(9)").text());

                $(".modal-title").html("Edit Car Details");
                $("#formdata").attr("action", "{{route('editcar')}}");
            });
        })
    </script>
    @endsection