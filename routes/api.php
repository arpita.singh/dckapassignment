<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('login', [App\Http\Controllers\API\AuthController::class, 'login']);


Route::post('/addcarapi', [App\Http\Controllers\CrudAPIController::class, 'addcarAPI'])->name('addcarapi');

Route::get('/details', [App\Http\Controllers\CrudAPIController::class, 'alldata'])->name('details');

Route::post('/edit', [App\Http\Controllers\CrudAPIController::class, 'EditAPI'])->name('edit');

Route::post('/delete', [App\Http\Controllers\CrudAPIController::class, 'deleteAPI'])->name('delete');
