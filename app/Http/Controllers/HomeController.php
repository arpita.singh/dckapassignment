<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\carinfo;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Http\Controllers\APICallerController;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        try {

            $url = '/api/details';
            $body = [];
            $response_data = self::makecall('get', $url, $body);
            $list = json_decode($response_data);
            $this->data['cardetail'] = $list->data;
            return view('home', $this->data);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function addCar(Request $request)
    {
        try {


            $url = '/api/addcarapi';
            $data = array(
                'name' => $request->name,
                'model' => $request->model,
                'owner' => $request->owner,
                'vehicle' => $request->vehicle,
                'address' => $request->address,
                'pincode' => $request->pincode,
                'city' => $request->city,
                'state' => $request->state,
                'country' => $request->country

            );

            if (count(array_filter($data)) > 0) {
                $body = ['cardetails' => json_encode($data)];
                $response_data = self::makecall('post', $url, $body);
                $response = json_decode($response_data);

                return Redirect()->route('home')->with('success', $response->msg);
            } else {
                return Redirect()->route('home')->with('error', "Car Details Should not be empty");
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
    }


    public function edit(Request $request)
    {
        try {

            $url = '/api/edit';
            $data = array(
                "id" => $request->id,
                'name' => $request->name,
                'model' => $request->model,
                'owner' => $request->owner,
                'vehicle' => $request->vehicle,
                'address' => $request->address,
                'pincode' => $request->pincode,
                'city' => $request->city,
                'state' => $request->state,
                'country' => $request->country

            );

            if ($request->id) {
                $body = ['cardetails' => json_encode($data)];
                $response_data = self::makecall('post', $url, $body);
                $response = json_decode($response_data);

                return Redirect()->route('home')->with('success', $response->msg);
            } else {
                return Redirect()->route('home')->with('error', "Something is missing");
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function delete(Request $request)
    {
        try {
            $url = '/api/delete';
            $body = ['id' => $request->id];

            if (count(array_filter($body)) > 0) {
                $response_data = self::makecall('post', $url, $body);
                $response = json_decode($response_data);
                return Redirect()->route('home')->with('success', $response->msg);
            } else {
                return Redirect()->route('home')->with('error', "Something went wrong");
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public static function makecall($method, $halfurl, $body)
    {

        $client = new Client();

        $url = url('/') . $halfurl;

        $headerData = ['Authorization' => auth()->user()->getRememberToken()];
        $param = ['headers' => $headerData, 'form_params' => $body];

        $res = $client->request($method, $url, $param);
        if ($res->getStatusCode() == 200) {
            $response_data = $res->getBody()->getContents();
        }

        return $response_data;
    }
}
