<?php

namespace App\Http\Controllers;

use App\Models\carinfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CrudAPIController extends Controller
{


    public function alldata(Request $request)
    {


        try {
            $data = array();

            if ($request->headers->has('Authorization')) {
                $checkrow = User::where('remember_token', $request->header('Authorization'))->first();
                if (!$checkrow) {
                    $data['status'] = 'Failed';
                    $data['msg'] = "Token do n't match";
                    return response()->json($data);
                }
            } else {
                $data['status'] = 'Failed';
                $data['msg'] = 'Token is missing';
                return response()->json($data);
            }

            $cardetails =  carinfo::all();

            if (count($cardetails) > 0) {
                $data['status'] = 'true';
                $data['msg'] = '';
                $data['data'] = $cardetails;
            } else {
                $data['status'] = 'Failed';
                $data['msg'] = 'Something Went Wrong';
                $data['data'] = '';
            }
            DB::commit();
            return response()->json($data);
        } catch (\Throwable $th) {
            throw $th;
            DB::rollback();
        }
    }


    public function addcarAPI(Request $request)
    {

        DB::beginTransaction();
        try {


            $car_detail = json_decode($request->cardetails);
            $data = array();


            if ($request->headers->has('Authorization')) {
                $checkrow = User::where('remember_token', $request->header('Authorization'))->first();
                if (!$checkrow) {
                    $data['status'] = 'Failed';
                    $data['msg'] = "Token do n't match";
                    return response()->json($data);
                }
            } else {
                $data['status'] = 'Failed';
                $data['msg'] = 'Token is missing';
                return response()->json($data);
            }



            if (
                empty($car_detail->name) && empty($car_detail->model) && empty($car_detail->owner) && empty($car_detail->vehicle) &&
                empty($car_detail->address)  && empty($car_detail->pincode) &&  empty($car_detail->city) && empty($car_detail->state) && empty($car_detail->country)
            ) {
                $data['status'] = 'Failed';
                $data['msg'] = 'At least One data should there';
                return response()->json($data);
            }


            $array = new carinfo();
            $array->company_name = $car_detail->name;
            $array->model = $car_detail->model;
            $array->owner_name = $car_detail->owner;
            $array->addressline = $car_detail->address;
            $array->pincode = $car_detail->pincode;
            $array->state = $car_detail->state;
            $array->city = $car_detail->city;
            $array->country = $car_detail->country;
            $array->vehicle_no = $car_detail->vehicle;
            $saved =  $array->save();
            if ($saved == true) {
                $data['status'] = 'true';
                $data['msg'] = 'Car Data Saved Successfully';
            } else {
                $data['status'] = 'Failed';
                $data['msg'] = 'Something Went Wrong';
            }
            DB::commit();
            return response()->json($data);
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
        }
    }



    public function EditAPI(Request $request)
    {

        try {
            $data = array();
            if ($request->headers->has('Authorization')) {
                $checkrow = User::where('remember_token', $request->header('Authorization'))->first();
                if (!$checkrow) {
                    $data['status'] = 'Failed';
                    $data['msg'] = "Token do n't match";
                    return response()->json($data);
                }
            } else {
                $data['status'] = 'Failed';
                $data['msg'] = 'Token is missing';
                return response()->json($data);
            }
            DB::beginTransaction();
            $car_detail = json_decode($request->cardetails);


            if (empty($car_detail->id)) {
                $data['status'] = 'Failed';
                $data['msg'] = 'ID is mandatory';
                return response()->json($data);
            }

            $checkrow = carinfo::find($car_detail->id);

            if ($checkrow) {

                $checkrow->company_name = $car_detail->name;
                $checkrow->model = $car_detail->model;
                $checkrow->owner_name = $car_detail->owner;
                $checkrow->addressline = $car_detail->address;
                $checkrow->pincode = $car_detail->pincode;
                $checkrow->state = $car_detail->state;
                $checkrow->city = $car_detail->city;
                $checkrow->country = $car_detail->country;
                $checkrow->vehicle_no = $car_detail->vehicle;
                $update =  $checkrow->save();
            }


            if ($update == true) {
                $data['status'] = 'true';
                $data['msg'] = 'Car Data Updatad Successfully';
            } else {
                $data['status'] = 'Failed';
                $data['msg'] = 'Something Went Wrong';
            }
            DB::commit();
            return response()->json($data);
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
        }
    }


    public function deleteAPI(Request $request)
    {
        try {
            $data = array();

            if ($request->headers->has('Authorization')) {
                $checkrow = User::where('remember_token', $request->header('Authorization'))->first();
                if (!$checkrow) {
                    $data['status'] = 'Failed';
                    $data['msg'] = "Token do n't match";
                    return response()->json($data);
                }
            } else {
                $data['status'] = 'Failed';
                $data['msg'] = 'Token is missing';
                return response()->json($data);
            }


            if (empty($request->id)) {
                $data['status'] = 'Failed';
                $data['msg'] = 'ID Should not be empty';
                return response()->json($data);
            }

            $checkrow = carinfo::find($request->id);
            $delete = $checkrow->delete();

            if ($delete == true) {
                $data['status'] = 'true';
                $data['msg'] = 'Car Details deleted';
            } else {
                $data['status'] = 'Failed';
                $data['msg'] = 'Something Went Wrong';
            }
            DB::commit();
            return response()->json($data);
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
        }
    }
}
